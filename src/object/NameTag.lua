--===========================================================================--
--  Dependencies
--===========================================================================--
local MathUtils 	= require 'src.math.MathUtils'

local round 		= MathUtils.Round


--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--
--	Class RPMMeter : HUD element
--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--
local NameTag = {}
NameTag.__index = NameTag


-------------------------------------------------------------------------------
--  NameTag:new : Creates a new name tag
-------------------------------------------------------------------------------
function NameTag:new( dyzkModel, name )
	local obj = {};
	
	obj.dyzkModel = dyzkModel;
	obj.name = name;
	obj.time = 0;
	
	return setmetatable( obj, self )
end


-------------------------------------------------------------------------------
--  NameTag:Update : Updates the name tag
-------------------------------------------------------------------------------
function NameTag:Update(ds)
	self.time = self.time + ds
end


-------------------------------------------------------------------------------
--  NameTag:Draw : Draws the name tag
-------------------------------------------------------------------------------
function NameTag:Draw()

	local showTime1 = 0.1
	local showTime2 = 0.7
	local stayTime  = 3.0
	local hideTime1 = 0.2
	local hideTime2 = 0.3
	
	if self.time >= showTime1 + showTime2 + stayTime + hideTime1 + hideTime2 then
		return
	end
	
	local x, y = self.dyzkModel:GetPosition();	
	local x2, y2 = 120, -120
	local x3, y3 = 500, 0
		
	
	if self.time < showTime1 then
		local t = self.time;
		love.graphics.line( x, y, x+x2*t, y+y2*t)
	elseif self.time < showTime1 + showTime2 then
		local t = (self.time - showTime1)/showTime2
		love.graphics.line( x, y, x+x2, y+y2, x+x2+x3*t, y+y2+y3*t )
	elseif self.time < showTime1 + showTime2 + stayTime then
		love.graphics.line( x, y, x+x2, y+y2, x+x2+x3, y+y2+y3 )
	elseif self.time < showTime1 + showTime2 + stayTime + hideTime1 then
		local t = (self.time - showTime1 - showTime2 - stayTime)/hideTime1
		love.graphics.line( x+x2*t, y+y2*t, x+x2, y+y2, x+x2+x3, y+y2+y3 )
	elseif self.time < showTime1 + showTime2 + stayTime + hideTime1 + hideTime2 then
		local t = (self.time - showTime1 - showTime2 - stayTime - hideTime1)/hideTime2
		love.graphics.line( x+x2+x3*t, y+y2+y3*t, x+x2+x3, y+y2+y3 )
	else
		--
	end
	
	if self.time > showTime1 + showTime2 then	
		love.graphics.print(self.name, x + 440, y - 380, 0, 8, 8);
	end
		
end


--===========================================================================--
--  Initialization
--===========================================================================--
return NameTag;